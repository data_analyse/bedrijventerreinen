-- Check BRT als alternatief
SELECT * FROM d_brt_top10nl.functioneelgebied WHERE typefunctioneelgebied LIKE '%bedrijventerrein%' 

-- Check geometrie
SELECT
ST_astext(geom)
FROM d_bedrijventerreinen.ibis_2019
limit 1;

-- Set WGS as geom_wgs
ALTER TABLE d_bedrijventerreinen.ibis_2019 RENAME COLUMN geom TO geom_wgs;

-- Add RD as geom
ALTER TABLE d_bedrijventerreinen.ibis_2019
	ADD COLUMN geom geometry(Geometry,28992);
	
-- Set RD geom
UPDATE d_bedrijventerreinen.ibis_2019 SET
  geom = ST_Transform(geom_wgs,28992)
FROM spatial_ref_sys
WHERE ST_SRID(geom_wgs) = srid;

-- test